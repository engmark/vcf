vcf
===

vCard find. Prints the vCards which are found with the given search terms.

Install
-------

    git clone --recursive https://gitlab.com/victor-engmark/vcf.git

Usage
-----

    ./vcf.sh jane ~/contacts/*.vcf

Find "jane" in all properties of all the vCards in `~/contacts`.
